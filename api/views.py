from django.shortcuts import render
import requests
# Create your views here.
from rest_framework.decorators import api_view
from rest_framework.response import Response
#from .serializers import TaskSerializer
from django.conf import settings
from .models import Task
# Create your views here.

@api_view(['POST'])
def apiLogin(request):

    headers = {
        'Content-Type': 'application/json',
    }

    

    data = request.body

    response = requests.post(settings.HOST+'/login/jwt', headers=headers, data=data)

    return Response(response)

@api_view(['POST'])
def apibanksAccounts(request):

    headers = {
        'Content-Type': 'application/json',
    }

    

    data = request.body

    response = requests.post(settings.HOST+'/banks/jwt/accounts', headers=headers, data=data)

    return Response(response)